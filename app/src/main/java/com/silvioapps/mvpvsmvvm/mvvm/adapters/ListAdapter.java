package com.silvioapps.mvpvsmvvm.mvvm.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.mvpvsmvvm.BR;
import com.silvioapps.mvpvsmvvm.R;
import com.silvioapps.mvpvsmvvm.mvvm.databinding.CountryListBaseObservable;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private List<CountryListBaseObservable> list = null;

    public void setData(List<CountryListBaseObservable> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mvvm_list_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        CountryListBaseObservable listBaseObservable = list.get(i);
        viewHolder.getBinding().setVariable(BR.country, listBaseObservable);
        viewHolder.getBinding().executePendingBindings();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding viewDataBinding = null;

        public ViewHolder(View view) {
            super(view);
            viewDataBinding = DataBindingUtil.bind(view);
        }

        public ViewDataBinding getBinding() {
            return viewDataBinding;
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }
}
