package com.silvioapps.mvpvsmvvm.mvvm.adapters;

import android.databinding.BindingAdapter;
import android.view.View;

public class BackgroundColorBindingAdapter {
    @BindingAdapter({"backgroundColor"})
    public static void backgroundColor(View view, int color) {
        view.setBackgroundColor(color);
    }
}