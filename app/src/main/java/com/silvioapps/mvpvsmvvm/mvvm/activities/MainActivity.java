package com.silvioapps.mvpvsmvvm.mvvm.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.silvioapps.mvpvsmvvm.mvvm.fragments.MainFragment;
import com.silvioapps.mvpvsmvvm.R;

public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mvvm_activity_main);

        if(savedInstanceState == null){
            attachFragment();
        }
    }

    public void attachFragment(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(R.id.frameLayout, new MainFragment());
            fragmentTransaction.commit();
        }
    }
}
