package com.silvioapps.mvpvsmvvm.mvvm.databinding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Color;
import com.silvioapps.mvpvsmvvm.BR;

public class CountryListBaseObservable extends BaseObservable{
    private String code = null;
    private String name = null;
    private int backgroundColor = -1;
    private int textColor = -1;

    public CountryListBaseObservable(String code, String name) {
        this.code = code;
        this.name = name;

        textColor = Color.BLACK;
        if(code.equals("BR")){
            backgroundColor = Color.YELLOW;
        }
        else if(code.equals("AR")){
            backgroundColor = Color.BLUE;
        }
        else if(code.equals("AU")){
            backgroundColor = Color.GREEN;
        }
        else if(code.equals("JP")){
            backgroundColor = Color.RED;
        }
        else if(code.equals("NZ")){
            backgroundColor = Color.BLACK;
            textColor = Color.WHITE;
        }
    }

    @Bindable
    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        this.code = value;
        notifyPropertyChanged(BR.code);
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int value) {
        this.backgroundColor = value;
        notifyPropertyChanged(BR.backgroundColor);
    }

    @Bindable
    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int value) {
        this.textColor = value;
        notifyPropertyChanged(BR.textColor);
    }

    @Bindable
    public String getLink() {
        return "<a href=http://easyautocomplete.com/resources/countries.json>Link</a>";
    }
}
