package com.silvioapps.mvpvsmvvm.mvvm.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.silvioapps.mvpvsmvvm.R;
import com.silvioapps.mvpvsmvvm.databinding.MvvmFragmentMainBinding;
import com.silvioapps.mvpvsmvvm.models.Country;
import com.silvioapps.mvpvsmvvm.mvvm.adapters.ListAdapter;
import com.silvioapps.mvpvsmvvm.mvvm.databinding.CountryListBaseObservable;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainFragment extends Fragment {
    private ListAdapter listAdapter = null;
    private MvvmFragmentMainBinding binding = null;
    private List<CountryListBaseObservable> countryListBaseObservableList = null;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.mvvm_fragment_main, viewGroup, false);
        View view = binding.getRoot();

        listAdapter = new ListAdapter();

        if(getActivity() != null) {
            TextView linkTextView = view.findViewById(R.id.linkTextView);
            linkTextView.setMovementMethod(LinkMovementMethod.getInstance());

            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

            RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(listAdapter);
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if(linearLayoutManager != null) {
                        int position = linearLayoutManager.findFirstVisibleItemPosition();

                        if (binding != null && countryListBaseObservableList != null && position > -1) {
                            binding.setCountry(countryListBaseObservableList.get(position));
                        }
                    }
                }
            });
        }

        getData();

        return view;
    }

    protected void getData(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://easyautocomplete.com/resources/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        Country.API service = retrofit.create(Country.API.class);
        Call<List<Country>> call = service.get();
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, retrofit2.Response<List<Country>> response) {
                countryListBaseObservableList = new ArrayList<>();

                for(Country country : response.body()){
                    CountryListBaseObservable countryListBaseObservable =
                            new CountryListBaseObservable(country.getCode(), country.getName());

                    countryListBaseObservableList.add(countryListBaseObservable);
                }

                if(listAdapter != null){
                    listAdapter.setData(countryListBaseObservableList);
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e("TAG***",t.getMessage());
            }
        });
    }
}
