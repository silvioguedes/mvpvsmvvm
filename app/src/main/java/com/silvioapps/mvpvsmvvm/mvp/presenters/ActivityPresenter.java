package com.silvioapps.mvpvsmvvm.mvp.presenters;

import android.os.Bundle;
import com.silvioapps.mvpvsmvvm.mvp.views.IActivityView;

public class ActivityPresenter implements IActivityPresenter {
    private IActivityView activityView = null;

    public ActivityPresenter(IActivityView activityView) {
        this.activityView = activityView;
    }

    @Override
    public void onCreate(Bundle bundle) {
        if (bundle == null && activityView != null) {
            activityView.attachFragment();
        }
    }
}