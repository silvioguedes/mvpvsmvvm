package com.silvioapps.mvpvsmvvm.mvp.utils;

import android.graphics.Color;
import android.widget.TextView;

/**
 * Created by silvio on 9/13/17.
 */

public class Utils {

    public static void setColors(String code, TextView textView){
        int textColor = Color.BLACK;
        int backgroundColor = -1;
        if(code.equals("BR")){
            backgroundColor = Color.YELLOW;
        }
        else if(code.equals("AR")){
            backgroundColor = Color.BLUE;
        }
        else if(code.equals("AU")){
            backgroundColor = Color.GREEN;
        }
        else if(code.equals("JP")){
            backgroundColor = Color.RED;
        }
        else if(code.equals("NZ")){
            backgroundColor = Color.BLACK;
            textColor = Color.WHITE;
        }

        textView.setTextColor(textColor);
        textView.setBackgroundColor(backgroundColor);
    }
}
