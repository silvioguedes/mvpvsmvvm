package com.silvioapps.mvpvsmvvm.mvp.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.silvioapps.mvpvsmvvm.mvp.fragments.MainFragment;
import com.silvioapps.mvpvsmvvm.mvp.presenters.ActivityPresenter;
import com.silvioapps.mvpvsmvvm.mvp.views.IActivityView;
import com.silvioapps.mvpvsmvvm.R;

public class MainActivity extends AppCompatActivity implements IActivityView{
    private ActivityPresenter activityPresenter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mvp_activity_main);

        activityPresenter = new ActivityPresenter(this);
        activityPresenter.onCreate(savedInstanceState);
    }

    @Override
    public void attachFragment(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(R.id.frameLayout, new MainFragment());
            fragmentTransaction.commit();
        }
    }
}
