package com.silvioapps.mvpvsmvvm.mvp.views;

import android.app.Activity;
import android.view.View;

public interface IFragmentView {
    void init(Activity activity, View view);

    void getData();
}