package com.silvioapps.mvpvsmvvm.mvp.presenters;

import android.app.Activity;
import android.view.View;

public interface IFragmentPresenter {
    void onCreateView(Activity activity, View view);
}