package com.silvioapps.mvpvsmvvm.mvp.presenters;

import android.app.Activity;
import android.view.View;
import com.silvioapps.mvpvsmvvm.mvp.views.IFragmentView;

public class FragmentPresenter implements IFragmentPresenter {
    private IFragmentView fragmentView = null;

    public FragmentPresenter(IFragmentView fragmentView) {
        this.fragmentView = fragmentView;
    }

    @Override
    public void onCreateView(Activity activity, View view) {
        if (activity != null && fragmentView != null) {
            fragmentView.init(activity, view);
            fragmentView.getData();
        }
    }
}