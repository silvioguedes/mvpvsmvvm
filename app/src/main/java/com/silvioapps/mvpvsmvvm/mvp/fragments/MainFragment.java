package com.silvioapps.mvpvsmvvm.mvp.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.silvioapps.mvpvsmvvm.R;
import com.silvioapps.mvpvsmvvm.models.Country;
import com.silvioapps.mvpvsmvvm.mvp.adapters.ListAdapter;
import com.silvioapps.mvpvsmvvm.mvp.presenters.FragmentPresenter;
import com.silvioapps.mvpvsmvvm.mvp.utils.Utils;
import com.silvioapps.mvpvsmvvm.mvp.views.IFragmentView;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainFragment extends Fragment implements IFragmentView{
    private ListAdapter listAdapter = null;
    private FragmentPresenter fragmentPresenter = null;
    private List<Country> countryList = null;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.mvp_fragment_main, viewGroup, false);

        fragmentPresenter = new FragmentPresenter(this);
        fragmentPresenter.onCreateView(getActivity(), view);

        return view;
    }

    @Override
    public void init(Activity activity, final View view) {
        listAdapter = new ListAdapter();

        TextView linkTextView = view.findViewById(R.id.linkTextView);
        linkTextView.setMovementMethod(LinkMovementMethod.getInstance());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            linkTextView.setText(Html.fromHtml("<a href=http://easyautocomplete.com/resources/countries.json>Link</a>",
                    Html.FROM_HTML_MODE_LEGACY));
        } else {
            linkTextView.setText(Html.fromHtml("<a href=http://easyautocomplete.com/resources/countries.json>Link</a>"));
        }

        final TextView textView = view.findViewById(R.id.bottomBar);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(listAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(linearLayoutManager != null) {
                    int position = linearLayoutManager.findFirstVisibleItemPosition();

                    if(countryList != null && position > -1){
                        Country country = countryList.get(position);

                        String text = getString(R.string.code) + " "+ country.getCode() + " " +
                                getString(R.string.name) + " " + country.getName();

                        if(textView != null) {
                            textView.setText(text);

                            Utils.setColors(country.getCode(), textView);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void getData(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://easyautocomplete.com/resources/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        Country.API service = retrofit.create(Country.API.class);
        Call<List<Country>> call = service.get();
        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, retrofit2.Response<List<Country>> response) {
                countryList = response.body();

                if(listAdapter != null){
                    listAdapter.setData(countryList);
                }
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e("TAG***",t.getMessage());
            }
        });
    }
}
