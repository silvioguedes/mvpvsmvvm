package com.silvioapps.mvpvsmvvm.mvp.views;

public interface IActivityView {
    void attachFragment();
}