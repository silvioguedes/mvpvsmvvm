package com.silvioapps.mvpvsmvvm.mvp.presenters;

import android.os.Bundle;

public interface IActivityPresenter {
    void onCreate(Bundle bundle);
}