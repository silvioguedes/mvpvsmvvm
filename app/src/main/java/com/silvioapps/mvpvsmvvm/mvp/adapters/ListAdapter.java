package com.silvioapps.mvpvsmvvm.mvp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.silvioapps.mvpvsmvvm.R;
import com.silvioapps.mvpvsmvvm.models.Country;
import com.silvioapps.mvpvsmvvm.mvp.utils.Utils;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{
    private List<Country> list = null;
    private Context context = null;

    public void setData(List<Country> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mvp_list_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String text = context.getString(R.string.code) + " "+ list.get(i).getCode() + " " +
                context.getString(R.string.name) + " " + list.get(i).getName();

        viewHolder.textView.setText(text);

        Utils.setColors(list.get(i).getCode(), viewHolder.textView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView = null;

        public ViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.textView);
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }
}
