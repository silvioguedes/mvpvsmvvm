package com.silvioapps.mvpvsmvvm.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public class Country{

	@SerializedName("code")
	private String code;

	@SerializedName("name")
	private String name;

    public interface API {
        @Headers({"Accept: application/json"})
        @GET("countries.json")
        Call<List<Country>> get();
    }

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}